﻿namespace NancyIntro
{
  using Nancy;

  public class HomeModule : NancyModule
  {
    public HomeModule()
    {
      Get["/"] = p => "Hello World!";
    }
  }
}